module.exports = class PostDataBaser {
  constructor(postModel) {
    this.m_postModel = postModel;
  }

  async getAllPosts() {
    const res = await this.m_postModel.find({});

    return res;
  }

  async addPost(post) {
    const newPost = new this.m_postModel(post);

    await newPost.save();
  }
};

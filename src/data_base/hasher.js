module.exports = class Hasher {
  constructor(bcrypt) {
    this.m_bcrypt = bcrypt;
  }

  saltAndHashAPassword(plainTextPassword) {
    const salt = this.m_bcrypt.genSaltSync(10);
    const hashedAndSaltedPassword = this.m_bcrypt.hashSync(
      plainTextPassword,
      salt
    );

    return hashedAndSaltedPassword;
  }

  verifyPassword(plainTextPassword, hashedPassword) {
    const res = this.m_bcrypt.compareSync(plainTextPassword, hashedPassword);

    return res;
  }
};

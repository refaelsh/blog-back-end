module.exports = class UserDataBaser {
  constructor(userModel, hasher) {
    this.m_userModel = userModel;
    this.m_hasher = hasher;
  }

  async findUser(username) {
    const users = await this.m_userModel.find({ username: username });

    if (users.length == 0) {
      return false;
    } else {
      return true;
    }
  }

  async addUser(username, plainTextPassword, email) {
    const hashedAndSaltedPassword =
      this.m_hasher.saltAndHashAPassword(plainTextPassword);

    const user = new this.m_userModel({
      username: username,
      password: hashedAndSaltedPassword,
      email: email,
    });

    await user.save();
  }

  async verifyUser(username, plainTextPassword) {
    const user = await this.m_userModel.find({ username: username });

    if (user.length == 0) {
      return false;
    }

    const hashedPassword = user[0].password;

    const res = this.m_hasher.verifyPassword(plainTextPassword, hashedPassword);

    return res;
  }
};

const express = require("express");

module.exports = class RegisterRouter {
  constructor(userDataBaser) {
    this.m_userDataBaser = userDataBaser;
  }

  getRouter() {
    const router = express.Router();

    const self = this;

    router.use(express.json());
    router.use(express.urlencoded({ extended: true }));

    router.post("/", async function (req, res) {
      const username = req.body.username;
      const password = req.body.password;
      const email = req.body.email;

      const result = await self.m_userDataBaser.findUser(username);

      if (result == false) {
        await self.m_userDataBaser.addUser(username, password, email);

        res.status(200);
        res.json({ message: "User added :-)" });
      } else {
        res.status(409);
        res.json({ message: "409" });
      }
    });

    return router;
  }
};

const express = require("express");

module.exports = class LoginRouter {
  constructor(userDataBaser, tokenizer) {
    this.m_userDataBaser = userDataBaser;
    this.m_tokenizer = tokenizer;
  }

  getRouter() {
    const router = express.Router();

    const self = this;

    router.post("/", async function (req, res) {
      const username = req.body.username;
      const password = req.body.password;

      const result = await self.m_userDataBaser.verifyUser(username, password);

      if (result == true) {
        res.status(200);

        const token = self.m_tokenizer.produceToken(username);

        res.json({ token: token });
      } else {
        res.status(401);
        res.json({ message: "Bad token or something else..." });
      }
    });

    return router;
  }
};

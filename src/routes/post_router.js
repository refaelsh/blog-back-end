const express = require("express");

module.exports = class PostRouter {
  constructor(postDataBaser, tokenVerifyingMiddleWare) {
    this.m_postDataBaser = postDataBaser;
    this.m_tokenVerifyingMiddleWare = tokenVerifyingMiddleWare;
  }

  getRouter() {
    const router = express.Router();

    const self = this;

    router.get("/", self.m_tokenVerifyingMiddleWare, async function (req, res) {
      const posts = await self.m_postDataBaser.getAllPosts();

      res.status(200);

      res.json(posts);
    });

    router.post(
      "/",
      self.m_tokenVerifyingMiddleWare,
      async function (req, res) {
        const username = req.headers.username;
        const title = req.headers.title;
        const contents = req.headers.contents;

        self.m_postDataBaser.addPost({
          username: username,
          title: title,
          contents: contents,
        });

        res.status(200);
        res.json({});
      }
    );

    return router;
  }
};

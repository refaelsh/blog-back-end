const express = require("express");

module.exports = class ForbiddenRouter {
  constructor(tokenVerifyingMiddleWare) {
    this.m_tokenVerifyingMiddleWare = tokenVerifyingMiddleWare;
  }

  getRouter() {
    const router = express.Router();

    const self = this;

    router.get("/", self.m_tokenVerifyingMiddleWare, async function (req, res) {
      const username = req.username;

      res.status(200);

      res.json({ username: username });
    });

    return router;
  }
};

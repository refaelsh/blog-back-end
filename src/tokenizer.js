module.exports = class Tokenizer {
  constructor(jwt) {
    this.m_jwt = jwt;
  }

  produceToken(username) {
    const secret = process.env.PRIVATE_KEY;
    const privateKey = [
      "-----BEGIN PRIVATE KEY-----",
      secret,
      "-----END PRIVATE KEY-----",
    ].join("\n");

    // Sign with RSA SHA256.
    const token = this.m_jwt.sign({ username: username }, privateKey, {
      algorithm: "RS256",
    });

    return token;
  }

  verifyToken(req, res, next) {
    const bearerHeader = req.headers["authorization"];

    if (bearerHeader === undefined) {
      res.status(401);
      res.json({ message: "bearerHeader was undefined" });
      return;
    }

    const token = bearerHeader.split(" ")[1];

    const secret = process.env.PUBLIC_KEY;
    const publicKey = [
      "-----BEGIN PUBLIC KEY-----",
      secret,
      "-----END PUBLIC KEY-----",
    ].join("\n");

    try {
      const decoded = this.m_jwt.verify(token, publicKey);

      req.username = decoded.username;

      next();
    } catch (e) {
      // console.log(e);
      res.status(401);
      res.json({ message: "Bad token!" });
    }
  }
};

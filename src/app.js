const express = require("express");
const compression = require("compression");
const helmet = require("helmet");
const bcrypt = require("bcryptjs");
const RegisterRouter = require("./routes/register_router.js");
const LoginRouter = require("./routes/login_router.js");
const ForbiddenRouter = require("./routes/forbidden_router.js");
const PostRouter = require("./routes/post_router.js");
const UserDataBaser = require("./data_base/user_data_baser.js");
const PostDataBaser = require("./data_base/post_data_baser.js");
const Hasher = require("./data_base/hasher.js");
const JsonWebToken = require("jsonwebtoken");
const Tokenizer = require("./tokenizer.js");
const User = require("./data_base/models/user_db_model");
const Post = require("./data_base/models/post_db_model");
const cors = require("cors");

var app = express();

app.use(cors());
// app.use(helmet());
// app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const version = `v1`;

const hasher = new Hasher(bcrypt);
const userDataBaser = new UserDataBaser(User, hasher);
const registerRouter = new RegisterRouter(userDataBaser);
app.use(`/api/` + version + `/register`, registerRouter.getRouter());

let tokenizer = new Tokenizer(JsonWebToken);
const loginRouter = new LoginRouter(userDataBaser, tokenizer);
app.use(`/api/` + version + `/login`, loginRouter.getRouter());

const forbiddenRouter = new ForbiddenRouter(
  tokenizer.verifyToken.bind(tokenizer)
);
app.use(`/api/` + version + `/forbidden`, forbiddenRouter.getRouter());

const postDataBaser = new PostDataBaser(Post);
const postRouter = new PostRouter(
  postDataBaser,
  tokenizer.verifyToken.bind(tokenizer)
);
app.use(`/api/` + version + `/posts`, postRouter.getRouter());

module.exports = app;

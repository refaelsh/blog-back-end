const request = require("supertest");
const Tokenizer = require("../tokenizer.js");
const jwt = require("jsonwebtoken");

jest.mock("jsonwebtoken");

const cut = new Tokenizer(jwt);

const username = "refaelsh";
const token = "some_signed_token";

beforeAll(() => {
  // console.log = function () {};
});

beforeEach(() => {
  jest.clearAllMocks();

  const secret = process.env.PUBLIC_KEY;
  const publicKey = [
    "-----BEGIN PUBLIC KEY-----",
    secret,
    "-----END PUBLIC KEY-----",
  ].join("\n");

  jest
    .spyOn(jwt, "verify")
    .mockImplementation(function (someToken, somePublicKey) {
      if (someToken == token && somePublicKey == publicKey) {
        return { username: username };
      } else {
        throw "Bla!";
      }
    });
});

test("Check token creation", async () => {
  const secret = process.env.PRIVATE_KEY;
  const privateKey = [
    "-----BEGIN PRIVATE KEY-----",
    secret,
    "-----END PRIVATE KEY-----",
  ].join("\n");

  jest
    .spyOn(jwt, "sign")
    .mockImplementation(function (payload, somePrivateKey, someAlgorithm) {
      if (
        JSON.stringify(payload) === JSON.stringify({ username: username }) &&
        somePrivateKey === privateKey &&
        JSON.stringify(someAlgorithm) === JSON.stringify({ algorithm: "RS256" })
      ) {
        return token;
      } else {
        return undefined;
      }
    });

  const res = cut.produceToken(username);

  expect(res).toBe(token);
});

test("Check good token verification", async () => {
  const req = {};
  req.headers = {};
  req.headers.authorization = `Bearer ` + token;

  let res = {};

  const next = jest.fn();

  cut.verifyToken(req, res, next);

  expect(req.username).toBe(username);

  expect(next.mock.calls.length).toBe(1);
});

test("Check bad token verification", async () => {
  const req = {};
  req.headers = {};
  req.headers.authorization = `Bearer ` + "some_bad_token";

  let res = jest.fn();
  res.status = jest.fn();
  res.json = jest.fn();

  const next = jest.fn();

  cut.verifyToken(req, res, next);

  expect(req.username).toBe(undefined);

  expect(next.mock.calls.length).toBe(0);

  expect(res.mock.calls.length).toBe(0);
  expect(res.status.mock.calls[0][0]).toBe(401);
});

test("Check no token given at all", async () => {
  const req = {};
  req.headers = {};
  req.headers.authorization = undefined;

  let res = jest.fn();
  res.status = jest.fn();
  res.json = jest.fn();

  const next = jest.fn();

  cut.verifyToken(req, res, next);

  expect(req.username).toBe(undefined);

  expect(next.mock.calls.length).toBe(0);

  expect(res.mock.calls.length).toBe(0);
  expect(res.status.mock.calls[0][0]).toBe(401);
});

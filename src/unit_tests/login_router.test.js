var express = require("express");
const request = require("supertest");
const LoginRouter = require("../routes/login_router.js");
const UserDataBaser = require("../data_base/user_data_baser.js");
const Tokenizer = require("../tokenizer.js");

jest.mock("../data_base/user_data_baser.js");
const userDataBaser = new UserDataBaser();

const tokenizer = new Tokenizer();

const loginRouter = new LoginRouter(userDataBaser, tokenizer);

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/login", loginRouter.getRouter());

const username = "refaelsh";
const nonExistentUsername = "some nonexistent username";
const password = "1234";
const wrongPassword = "some wrong password";

beforeAll(() => {
  // console.log = function () {};
});

beforeEach(() => {
  jest.clearAllMocks();

  jest
    .spyOn(UserDataBaser.prototype, "verifyUser")
    .mockImplementation(function (some_username, some_password) {
      if (some_username === username && some_password === password) {
        return true;
      } else {
        return false;
      }
    });

  jest
    .spyOn(Tokenizer.prototype, "produceToken")
    .mockImplementation(function (some_username) {
      if (some_username === username) {
        return "some_token_goes_here";
      }
    });
});

test("Correct username and correct password", async () => {
  await request(app)
    .post("/login")
    .send({ username: username, password: password })
    .expect("Content-Type", /json/)
    .expect(200)
    .then((res) => {
      expect(res.body).toStrictEqual({
        token: "some_token_goes_here",
      });
    });
});

test("Correct username and wrong password", async () => {
  await request(app)
    .post("/login")
    .set({ username: username, password: wrongPassword })
    .expect(401);
});

test("Wrong username and correct password", async () => {
  await request(app)
    .post("/login")
    .set({ username: nonExistentUsername, password: password })
    .expect(401);
});

test("Wrong username and wrong password", async () => {
  await request(app)
    .post("/login")
    .set({ username: nonExistentUsername, password: wrongPassword })
    .expect(401);
});

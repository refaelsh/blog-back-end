var express = require("express");
const request = require("supertest");
const Mocks = require("./mocks.js");
const ForbiddenRouter = require("../routes/forbidden_router.js");

const forbiddenRouter = new ForbiddenRouter(Mocks.verifyToken);

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/forbidden", forbiddenRouter.getRouter());

const tokenPayload = Mocks.tokenPayload;
const token = Mocks.token;

beforeAll(() => {});

beforeEach(() => {
  jest.clearAllMocks();
});

test("No token at all", async () => {
  await request(app).get("/forbidden").expect(401);
});

test("Correct token", async () => {
  await request(app)
    .get("/forbidden")
    .auth(token, { type: "bearer" })
    .expect("Content-Type", /json/)
    .expect(200, { username: tokenPayload });
});

test("Wrong token", async () => {
  await request(app)
    .get("/forbidden")
    .auth("some_wrong_token", { type: "bearer" })
    .expect(401);
});

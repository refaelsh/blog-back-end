var express = require("express");
const request = require("supertest");
const RegisterRouter = require("../routes/register_router.js");
const DataBaser = require("../data_base/user_data_baser.js");

jest.mock("../data_base/user_data_baser.js");
const userDataBaser = new DataBaser();

const registerRouter = new RegisterRouter(userDataBaser);

const username = "refaelsh";
const password = "1234";
const email = "bla@gmail.com";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/register", registerRouter.getRouter());

beforeAll(() => {
  // console.log = function () {};
});

beforeEach(() => {
  jest.clearAllMocks();
});

test("New user addition", async () => {
  userDataBaser.findUser.mockReturnValue(false);

  await request(app)
    .post("/register")
    .send({ username: username, password: password, email: email })
    .expect(200);

  expect(userDataBaser.addUser.mock.calls.length).toBe(1);
  expect(userDataBaser.addUser.mock.calls[0][0]).toBe(username);
  expect(userDataBaser.addUser.mock.calls[0][1]).toBe(password);
  expect(userDataBaser.addUser.mock.calls[0][2]).toBe(email);
});

test("Existing user", async () => {
  userDataBaser.findUser.mockReturnValue(true);

  await request(app)
    .post("/register")
    .set({ username: username, password: password, email: email })
    .expect(409);

  expect(userDataBaser.addUser.mock.calls.length).toBe(0);
});

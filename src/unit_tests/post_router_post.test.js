const express = require("express");
const request = require("supertest");
const Mocks = require("./mocks.js");
const PostRouter = require("../routes/post_router.js");
const PostDataBaser = require("../data_base/post_data_baser.js");

jest.mock("../data_base/post_data_baser.js");
const postDataBaser = new PostDataBaser();

const postRouter = new PostRouter(postDataBaser, Mocks.verifyToken);

const tokenPayload = Mocks.tokenPayload;
const token = Mocks.token;

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/posts", postRouter.getRouter());

const username = "refaelsh";
const contents = "some_contents";
const title = "some_title";
const post = { username: username, title: title, contents: contents };

beforeAll(() => {
  console.log = function () {};
});

beforeEach(() => {
  jest.clearAllMocks();
});

test("Post addition", async () => {
  postDataBaser.addPost = jest.fn();

  await request(app)
    .post("/posts")
    .auth(token, { type: "bearer" })
    .set(post)
    .expect("Content-Type", /json/)
    .expect(200);

  expect(postDataBaser.addPost.mock.calls.length).toBe(1);
  expect(postDataBaser.addPost.mock.calls[0][0]).toStrictEqual(post);
});

test("Correct token", async () => {
  await request(app)
    .post("/posts")
    .auth(token, { type: "bearer" })
    .expect("Content-Type", /json/)
    .expect(200);
});

test("Wrong token", async () => {
  await request(app)
    .post("/posts")
    .auth("some_wrong_token", { type: "bearer" })
    .expect(401);
});

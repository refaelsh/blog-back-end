const PostDataBaser = require("../data_base/post_data_baser.js");
const Post = require("../data_base/models/post_db_model.js");

jest.mock("../data_base/models/post_db_model.js");

const cut = new PostDataBaser(Post);

const posts = [
  { title: "some_title", contents: "some_contents" },
  { title: "some_title_2", contents: "some_contents_2" },
];

beforeAll(() => {
  console.log = function () {};

  jest.spyOn(Post, "find").mockImplementation(function (filter) {
    if (JSON.stringify(filter) === JSON.stringify({})) {
      return posts;
    } else {
      return [];
    }
  });
});

beforeEach(() => {
  jest.clearAllMocks();
});

test("Get all posts", async () => {
  const res = await cut.getAllPosts();

  expect(res).toBe(posts);
});

test("Add a post", async () => {
  const some_mock = jest.fn();
  some_mock.save = jest.fn();

  Post.mockReturnValue(some_mock);

  const post = { title: "some_title", contents: "some_contents" };

  const res = await cut.addPost(post);

  expect(Post.mock.calls.length).toBe(1);
  expect(Post.mock.calls[0][0]).toStrictEqual(post);

  expect(some_mock.save.mock.calls.length).toBe(1);
});

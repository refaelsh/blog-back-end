const Hasher = require("../data_base/hasher.js");
const bcrypt = require("bcryptjs");

jest.mock("bcryptjs");

const cut = new Hasher(bcrypt);

const salt = "some_salt";
const password = "some_password_for_hashing";
const hashedAndSaltedPassword = "hashed_ans_salted_password";

beforeAll(() => {
  // console.log = function () {};
});

beforeEach(() => {
  jest.clearAllMocks();
});

test("Salting and hashing test", () => {
  jest.spyOn(bcrypt, "genSaltSync").mockImplementation(function (rounds) {
    if (rounds == 10) {
      return salt;
    }
  });

  jest
    .spyOn(bcrypt, "hashSync")
    .mockImplementation(function (somePlainTextPassword, someSalt) {
      if (somePlainTextPassword == password && someSalt == salt) {
        return hashedAndSaltedPassword;
      }
    });

  const res = cut.saltAndHashAPassword(password);

  expect(res).toBe(hashedAndSaltedPassword);
});

test("Verify hashed password", () => {
  jest
    .spyOn(bcrypt, "compareSync")
    .mockImplementation(function (
      somePlainTextPassword,
      someHashedAndSaltedPassword
    ) {
      if (
        somePlainTextPassword == password &&
        someHashedAndSaltedPassword == hashedAndSaltedPassword
      ) {
        return true;
      }
    });

  const res = cut.verifyPassword(password, hashedAndSaltedPassword);

  expect(res).toBe(true);
});

const tokenizerObject = {};
tokenizerObject.verifyToken = jest.fn();
tokenizerObject.token = "some_signed_token";
tokenizerObject.tokenPayload = "some_token_payload";

jest
  .spyOn(tokenizerObject, "verifyToken")
  .mockImplementation(function (req, res, next) {
    const bearerHeader = req.headers["authorization"];
    if (bearerHeader === undefined) {
      res.sendStatus(401);
      return;
    }

    const someToken = bearerHeader.split(" ")[1];

    if (someToken == tokenizerObject.token) {
      req.username = tokenizerObject.tokenPayload;
      next();
    } else {
      res.sendStatus(401);
    }
  });

module.exports = tokenizerObject;

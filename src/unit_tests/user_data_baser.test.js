const Hasher = require("../data_base/hasher.js");
const UserDataBaser = require("../data_base/user_data_baser.js");
const User = require("../data_base/models/user_db_model.js");

jest.mock("../data_base/hasher.js");
const hasher = new Hasher();

jest.mock("../data_base/models/user_db_model.js");

const cut = new UserDataBaser(User, hasher);

const username = "refaelsh";
const nonExistentUsername = "some nonexistent username";
const password = "1234";
const hashedPassword = `hashed` + password;
const wrongPassword = "some wrong password";
const email = "bla@gmail.com";

beforeAll(() => {
  // console.log = function () {};

  jest
    .spyOn(Hasher.prototype, "saltAndHashAPassword")
    .mockImplementation(function (some_password) {
      if (some_password == password) {
        return hashedPassword;
      } else {
        return "some wrong password but hashed";
      }
    });

  jest
    .spyOn(Hasher.prototype, "verifyPassword")
    .mockImplementation(function (plainTextPassword, someHashedPassword) {
      if (
        plainTextPassword == password &&
        someHashedPassword == hashedPassword
      ) {
        return true;
      } else {
        return false;
      }
    });

  jest.spyOn(User, "find").mockImplementation(function (filter) {
    if (JSON.stringify(filter) === JSON.stringify({ username: username })) {
      return [
        {
          _id: "615c99df2bc8f395b08e5ac0",
          username: "refaelsh",
          password: hashedPassword,
          email: "bla@gmail_29",
          __v: 0,
        },
      ];
    } else {
      return [];
    }
  });
});

beforeEach(() => {
  jest.clearAllMocks();
});

test("Find existing user", async () => {
  const res = await cut.findUser(username);

  expect(res).toBe(true);
});

test("Find none existing user", async () => {
  const res = await cut.findUser(nonExistentUsername);

  expect(res).toBe(false);
});

test("Add user", async () => {
  const some_mock = jest.fn();
  some_mock.save = jest.fn();

  User.mockReturnValue(some_mock);

  const res = await cut.addUser(username, password, email);

  expect(User.mock.calls.length).toBe(1);
  expect(User.mock.calls[0][0]).toStrictEqual({
    username: username,
    password: hashedPassword,
    email: email,
  });

  expect(some_mock.save.mock.calls.length).toBe(1);
});

test("Correct username and correct password", async () => {
  const res = await cut.verifyUser(username, password);

  expect(res).toBe(true);
});

test("Correct username and wrong password", async () => {
  const res = await cut.verifyUser(username, wrongPassword);

  expect(res).toBe(false);
});

test("Wrong username and correct password", async () => {
  const res = await cut.verifyUser(nonExistentUsername, password);

  expect(res).toBe(false);
});

test("Wrong username and wrong password", async () => {
  const res = await cut.verifyUser(nonExistentUsername, wrongPassword);

  expect(res).toBe(false);
});

const express = require("express");
const request = require("supertest");
const PostRouter = require("../routes/post_router.js");
const PostDataBaser = require("../data_base/post_data_baser.js");

jest.mock("../data_base/post_data_baser.js");
const postDataBaser = new PostDataBaser();

const tokenizerObject = {};
tokenizerObject.verifyToken = jest.fn();

const postRouter = new PostRouter(postDataBaser, tokenizerObject.verifyToken);

const tokenPayload = "some_token_payload";
const token = "some_signed_token";

const posts = [
  { title: "some_title", contents: "some_contents" },
  { title: "some_title_2", contents: "some_contents_2" },
];

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/posts", postRouter.getRouter());

beforeAll(() => {
  jest
    .spyOn(tokenizerObject, "verifyToken")
    .mockImplementation(function (req, res, next) {
      const bearerHeader = req.headers["authorization"];

      if (bearerHeader === undefined) {
        res.sendStatus(401);
        return;
      }

      const someToken = bearerHeader.split(" ")[1];

      if (someToken == token) {
        req.username = tokenPayload;
        next();
      } else {
        res.sendStatus(401);
      }
    });

  jest
    .spyOn(PostDataBaser.prototype, "getAllPosts")
    .mockImplementation(function () {
      return posts;
    });
});

beforeEach(() => {
  jest.clearAllMocks();
});

test("Correct token", async () => {
  await request(app)
    .get("/posts")
    .auth(token, { type: "bearer" })
    .expect("Content-Type", /json/)
    .expect(200, posts);
});

test("Wrong token", async () => {
  await request(app)
    .get("/posts")
    .auth("some_wrong_token", { type: "bearer" })
    .expect(401);
});

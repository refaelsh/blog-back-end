const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config();

const connectionString = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.2vtxf.mongodb.net/db-for-blog?retryWrites=true&w=majority`;
async function run() {
  await mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  console.log(`Database connected!`);
}
run().catch(console.dir);

:set spellfile=./en.utf-8.add
:set backupcopy=yes

nnoremap <F7> :wa <bar> :compiler jest <bar> :make <CR>
" nnoremap <F7> :wa <bar> :!npm run test<CR>
nnoremap <F8> :wa <bar> :term jest --coverage <CR>
